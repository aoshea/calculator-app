import React from "react";
import "./Calculator.css";

import CalculatorButton from "./CalculatorButton";
import EELogo from "./EELogo";

interface CalcState {
  num: string;
  next: string;
  operator: string;
}

function calculate(a: string, operator: string, b: string): number {
  const numA = parseFloat(a);
  const numB = parseFloat(b);
  switch (operator) {
    case "+":
      return numA + numB;
    case "-":
      return numA - numB;
    case "÷":
      return numA / numB;
    case "×":
      return numA * numB;
    default:
      throw new Error("Invalid operator" + operator);
  }
}

function Calculator() {
  const [state, setState] = React.useState<CalcState>({
    num: "",
    next: "",
    operator: ""
  });

  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    const target = e.target as HTMLButtonElement;
    const textContent = target.textContent || "";
    const newState = { ...state };

    if (/\d/.test(textContent)) {
      if (state.operator === "") {
        newState.num = state.num + textContent;
      } else {
        newState.next = state.next + textContent;
      }
    }

    if (textContent === ".") {
      if (state.operator !== "") {
        newState.next = state.next.includes(".")
          ? state.next
          : state.next + ".";
      } else {
        newState.num = state.num.includes(".") ? state.num : state.num + ".";
      }
    }

    if (/[-+÷×]/.test(textContent)) {
      if (state.next !== "" && state.operator !== "") {
        newState.num = calculate(state.num, state.operator, state.next) + "";
        newState.next = "";
      }
      newState.operator = textContent;
    }

    if (textContent === "=") {
      if (state.operator !== "" && state.next !== "") {
        newState.num = calculate(state.num, state.operator, state.next) + "";
        newState.operator = "";
        newState.next = "";
      }
    }

    if (textContent === "AC") {
      newState.num = "";
      newState.next = "";
      newState.operator = "";
    }

    setState(newState);
  };

  return (
    <div className="Calculator Calculator-layout">
      <EELogo />
      <input
        className="Calculator-display"
        type="text"
        readOnly
        value={state.next || state.num}
      ></input>
      <CalculatorButton onClick={handleClick}>AC</CalculatorButton>
      <CalculatorButton onClick={handleClick}>0</CalculatorButton>
      <CalculatorButton onClick={handleClick}>1</CalculatorButton>
      <CalculatorButton onClick={handleClick}>2</CalculatorButton>
      <CalculatorButton onClick={handleClick}>3</CalculatorButton>
      <CalculatorButton onClick={handleClick}>4</CalculatorButton>
      <CalculatorButton onClick={handleClick}>5</CalculatorButton>
      <CalculatorButton onClick={handleClick}>6</CalculatorButton>
      <CalculatorButton onClick={handleClick}>7</CalculatorButton>
      <CalculatorButton onClick={handleClick}>8</CalculatorButton>
      <CalculatorButton onClick={handleClick}>9</CalculatorButton>
      <CalculatorButton isActive={state.operator === "+"} onClick={handleClick}>
        +
      </CalculatorButton>
      <CalculatorButton isActive={state.operator === "-"} onClick={handleClick}>
        -
      </CalculatorButton>
      <CalculatorButton isActive={state.operator === "÷"} onClick={handleClick}>
        ÷
      </CalculatorButton>
      <CalculatorButton isActive={state.operator === "×"} onClick={handleClick}>
        ×
      </CalculatorButton>
      <CalculatorButton onClick={handleClick}>.</CalculatorButton>
      <CalculatorButton onClick={handleClick}>=</CalculatorButton>
    </div>
  );
}

export default Calculator;
