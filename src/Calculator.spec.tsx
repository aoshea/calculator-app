import React from "react"; import { render, screen, fireEvent } from "@testing-library/react";
import Calculator from "./Calculator";

describe("Calculator", () => {
  beforeEach(() => {
    render(<Calculator />);
  });

  it.each(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])(
    "should render number button %s",
    num => {
      const element = screen.getByRole("button", { name: num });
      expect(element).toBeInTheDocument();
    }
  );

  it.each(["+", "-", "÷", "×"])(
    "should render operator button %s",
    operator => {
      const element = screen.getByRole("button", { name: operator });
      expect(element).toBeInTheDocument();
    }
  );

  it("should render equals button", () => {
    const element = screen.getByRole("button", { name: "=" });
    expect(element).toBeInTheDocument();
  });

  it("should display numbers inputted", () => {
    const element = screen.getByRole("textbox");
    fireEvent.click(screen.getByRole("button", { name: "1" }));
    fireEvent.click(screen.getByRole("button", { name: "2" }));
    fireEvent.click(screen.getByRole("button", { name: "3" }));
    expect(element).toHaveValue("123");
  });

  it.each([
    ["2", "+", "3", "5"],
    ["4", "-", "1", "3"],
    ["5", "×", "2", "10"],
    ["6", "÷", "2", "3"]
  ])(
    "should calculate %s %s %s result on pressing equals",
    (a, operator, b, expected) => {
      const element = screen.getByRole("textbox");
      fireEvent.click(screen.getByRole("button", { name: a }));
      fireEvent.click(screen.getByRole("button", { name: operator }));
      fireEvent.click(screen.getByRole("button", { name: b }));
      fireEvent.click(screen.getByRole("button", { name: "=" }));
      expect(element).toHaveValue(expected);
    }
  );

  it.each([
    ["1", "+", "2", "×", "3", "9"],
    ["5", "-", "1", "÷", "2", "2"]
  ])(
    "should calculate (%s %s %s) %s %s",
    (a, operator1, b, operator2, c, expected) => {
      const element = screen.getByRole("textbox");
      fireEvent.click(screen.getByRole("button", { name: a }));
      fireEvent.click(screen.getByRole("button", { name: operator1 }));
      fireEvent.click(screen.getByRole("button", { name: b }));
      fireEvent.click(screen.getByRole("button", { name: operator2 }));
      fireEvent.click(screen.getByRole("button", { name: c }));
      fireEvent.click(screen.getByRole("button", { name: "=" }));
      expect(element).toHaveValue(expected);
    }
  );

  it("should render AC button", () => {
    const element = screen.getByRole("button", { name: "AC" });
    expect(element).toBeInTheDocument();
  });

  it("should clear calculator on pressing AC", () => {
    const element = screen.getByRole("textbox");
    fireEvent.click(screen.getByRole("button", { name: "9" }));
    fireEvent.click(screen.getByRole("button", { name: "9" }));
    fireEvent.click(screen.getByRole("button", { name: "9" }));
    expect(element).toHaveValue("999");
    fireEvent.click(screen.getByRole("button", { name: "AC" }));
    expect(element).toHaveValue("");
  });

  it("should display current num input after operator input", () => {
    const element = screen.getByRole("textbox");
    fireEvent.click(screen.getByRole("button", { name: "1" }));
    fireEvent.click(screen.getByRole("button", { name: "2" }));
    fireEvent.click(screen.getByRole("button", { name: "3" }));
    expect(element).toHaveValue("123");
    fireEvent.click(screen.getByRole("button", { name: "-" }));
    fireEvent.click(screen.getByRole("button", { name: "4" }));
    fireEvent.click(screen.getByRole("button", { name: "5" }));
    fireEvent.click(screen.getByRole("button", { name: "6" }));
    expect(element).toHaveValue("456");
  });

  it("should show operator as active", () => {
    const operator = screen.getByRole("button", { name: "÷" });
    fireEvent.click(screen.getByRole("button", { name: "4" }));
    fireEvent.click(operator);
    expect(operator).toHaveClass("Calculator-button--active");
    fireEvent.click(screen.getByRole("button", { name: "2" }));
    fireEvent.click(screen.getByRole("button", { name: "=" }));
    expect(operator).not.toHaveClass("Calculator-button--active");
  });

  it("should render a decimal button", () => {
    const element = screen.getByRole("button", { name: "." });
    expect(element).toBeInTheDocument();
  });

  it.each([
    [".", "2", "5", "×", "4", "0", "10"],
    ["1", ".", "5", "×", "2", "4", "36"]
  ])(
    "should calculate result when using decimal button (%s%s%s %s %s%s)",
    (a, b, c, operator, d, e, expected) => {
      const element = screen.getByRole("textbox");
      fireEvent.click(screen.getByRole("button", { name: a }));
      fireEvent.click(screen.getByRole("button", { name: b }));
      fireEvent.click(screen.getByRole("button", { name: c }));
      fireEvent.click(screen.getByRole("button", { name: operator }));
      fireEvent.click(screen.getByRole("button", { name: d }));
      fireEvent.click(screen.getByRole("button", { name: e }));
      fireEvent.click(screen.getByRole("button", { name: "=" }));
      expect(element).toHaveValue(expected);
    }
  );

  it("should render the equal experts logo", () => {
    const element = screen.getByRole("img", { name: "Equal Experts Logo" });
    expect(element).toBeInTheDocument();
  });
});
