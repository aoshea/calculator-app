import React from "react";
import classNames from "classnames";

type CalculatorButtonProps = {
  isActive?: boolean;
} & React.ComponentPropsWithoutRef<"button">;

const CalculatorButton: React.FC<CalculatorButtonProps> = ({
  children,
  isActive,
  ...buttonProps
}) => {
  return (
    <button
      className={classNames(
        "Calculator-button",
        isActive && "Calculator-button--active"
      )}
      type="button"
      {...buttonProps}
    >
      {children}
    </button>
  );
};

export default CalculatorButton;
